# Use the official Node.js image with a specific version
FROM node:14.19.0

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install application dependencies
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Expose the port on which your app will run
# Adjust the port if your app runs on a different port
EXPOSE 6002

# Set environment variables
ENV PORT=6002
ENV MONGO_URL=mongodb://mongo:27017/yourdbname
ENV JWT_SEC=yourjwtsecret
ENV SECRET=yoursecret

# Start the application
CMD ["node", "server.js"]
